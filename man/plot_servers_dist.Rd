% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/funs.R
\name{plot_servers_dist}
\alias{plot_servers_dist}
\title{Visualise results}
\usage{
plot_servers_dist(df)
}
\arguments{
\item{df}{A \code{tibble} returned by \code{get_df_chart_cc} function.}
}
\value{
A \code{ggplot2} plot.
}
\description{
Generates a plot.
}
\examples{
library(dnsrttvis)
get_df_chart_cc(df_traffic, df_rtt_aggr, 10) \%>\% plot_servers_dist()
}
