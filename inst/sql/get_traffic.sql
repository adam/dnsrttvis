SELECT
        src,
        server,
        ipv,
        COUNT(*) as cnt,
        SUM(len) as sum_len,
        SUM(res_len) as sum_res_len
FROM
        ${var:tb}
WHERE
        year=${var:year} AND
        month=${var:month} AND
        day=${var:day}
GROUP BY
        server,
        src,
        ipv