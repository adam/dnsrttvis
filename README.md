# dnsrttvis

`dnsrttvis` - R package for DNS traffic visualisation

## Requirements

Besides R packages specified in [DESCRIPTION](DESCRIPTION) this library may require:

  * [Maxmid GeoIP2 Country database](https://www.maxmind.com/en/geoip2-country-database) (commercial) or [Maxmid GeoLite2  Country database](https://dev.maxmind.com/geoip/geoip2/geolite2/) (free)

## Installation

```R
install.packages("devtools")
devtools::install_git("https://gitlab.labs.nic.cz/adam/dnsrttvis")
```

## Getting data

You need two files providing information about the [sources of DNS quries](#sources-of-dns-queries) and [RTT of DNS queries](#rtt-of-dns-queries).

### Sources of DNS queries

For each country we need information about the number of queries to a particular DNS server. It should be provided in a CSV file containing columns as described below.

| field          | description                      |
| -------------- | -------------------------------- |
| `country_code` | *ISO 3166-1 alfa-2* country code |
| `server`       |  server name                     |
| `ipv`          |  IP version (4 or 6)             |
| `n`            |  number of DNS queries           |
| `sum_len`      |  total size of DNS queries       |
| `sum_res_len`  |  total size of DNS responses     |


for example:

```
"country_code";"server";"ipv";"n";"cnt";"sum_len";"sum_res_len"
"CZ";"cra-cz";4;16201;431570857;36005119118;151203107943
"CZ";"cra-cz";6;1168;260459651;28100752952;99176270769
"CZ";"vix-cz";4;36276;756507789;64818637090;274702277058
"CZ";"vix-cz";6;1638;48746767;5010805318;18819860839
"DE";"decix-cz";4;71329;5118039;434224555;1786602269
"DE";"decix-cz";6;24183;22369999;2371541508;8272291763
NA;"cecolo-cz";6;306;398995;44491968;176714104
```
In order to generate such a CSV file you may use scripts provided with this library:

1. Locate scripts: 
```console
ROOT=`Rscript -e 'cat(find.package("dnsrttvis"))'`
```

2. Get datafrom Impala table (an example for DNS traffic from 1st August 2019 saved to `dns.queries` table of [Entrada data model](https://entrada.sidnlabs.nl/datamodel/))
```console
impala-shell -f $ROOT/sql/get_traffic.sql --var=tb=dns.queries --var=year=2019 --var=month=8 --var=day=1 --print_header -B --output_delimiter=';'  -o sources-raw.csv
```

3. Add GeoIP information and aggregate sources by country code (make sure that there is correct path to GeoIP database in aggregate.R source code):
```console
$ROOT/exec/aggregate.R --input sources-raw.csv --output sources.csv --method country_code
```

### RTT of DNS queries

For each TCP connection we need information about RTT of a TCP handshake. An input should be a CSV file containing columns described below (without header).

| field       | description                                             |
| ----------- | ------------------------------------------------------- |
| `timestamp` | unixtime                                                |
| `src`       | client IP address                                       | 
| `rtt`       | RTT of a TCP handshake (seconds)                        |
| `cc`        | *ISO 3166-1 alfa-2* country code for client IP  address |
| `asn`       | AS number for client IP address                         |
| `server`    | server name                                             |
| `ipv`       | IP version (ipv4 or ipv6)                               |

for example:

```
1557878362.402873000;2001:xxxx::2;0.109335000;US;2914;vix-cz;ipv6
1557878368.248270000;2001:xxxx::5dcc;0.033565000;ES;766;vix-cz;ipv6
1557878376.029747000;2600:xxxx::302;0.166221000;US;10912;vix-cz;ipv6
1557878388.268282000;2400:xxxx::ac45:68;0.000641000;AT;13335;vix-cz;ipv6
1557878436.155076000;2001:xxxx::2;0.130153000;US;2914;vix-cz;ipv6
```

To get RTT of a TCP handshake from a PCAP file you may use [`tshark` tool](https://www.wireshark.org/docs/man-pages/tshark.html) along with scripts provided with this library. For each PCAP file you need to repeat the following steps

1. Extract TCP handshake RTT for all TCP sessions saved in a PCAP file. Below there is an example of `tshark` rules for .CZ domain name
```console
# input file (a PCAP saved on cra-cz server):
PCAP=cra-cz.20190801.000216.000000
# output files (a temporary file):
RTT4=`mktemp`
RTT6=`mktemp`
tshark -r $PCAP -2 -T fields -R 'tcp.flags.ack==1 && tcp.analysis.ack_rtt &&  tcp.analysis.initial_rtt && tcp.flags.reset==0 && tcp.flags.fin==0 && (ip.dst==194.0.12.1 || ip.dst==194.0.13.1 || ip.dst==194.0.14.1 || ip.dst==193.29.206.1) && !tcp.analysis.retransmission && tcp.ack==1'  -e frame.time_epoch -e 'ip.src' -e 'tcp.analysis.initial_rtt' -E separator=";" > $RTT4 2>/dev/null
tshark -r $PCAP -2 -T fields -R 'tcp.flags.ack==1 && tcp.analysis.ack_rtt &&  tcp.analysis.initial_rtt && tcp.flags.reset==0 && tcp.flags.fin==0 && (ipv6.dst==2001:678:f::1 || ipv6.dst==2001:678:10::1 || ipv6.dst==2001:678:11::1 || ipv6.dst==2001:678:1::1) && !tcp.analysis.retransmission && tcp.ack==1'  -e frame.time_epoch -e 'ipv6.src' -e 'tcp.analysis.initial_rtt' -E separator=";" > $RTT6 2>/dev/null
```

2. Add geoIP data (make sure that there is correct path to GeoIP database in enrich.py source code)
```console
ROOT=`Rscript -e 'cat(find.package("dnsrttvis"))'`
SERVER=${PCAP%%.*}
$ROOT/exec/enrich.py $RTT4 ${PCAP}.4.csv $SERVER ipv4
$ROOT/exec/enrich.py $RTT6 ${PCAP}.6.csv $SERVER ipv6
rm $RTT4
rm $RTT6
```

After processing all PCAP files you need to merge the results into one file:
```console
cat *.{4,6}.csv > rtt.csv
```


## Plotting charts

In order to plot the charts run the folloring code in R.

```R
library(dplyr)
library(readr)
library(countrycode)
library(ggplot2)
library(dnsrttvis)

df_rtt <- read_rtt("rtt.csv") %>% 
  rtt_src_median()

df_traffic <- read_traffic("sources.csv")

# get chart for top 20 countries
df_chart_cc <- get_df_chart_cc(df_traffic, df_rtt, 20)

p <- plot_servers_dist(df_chart_cc) +
  ggtitle("DNS trafffic distribution vs evaluated RTT for top 20 countries")
  
p
```
![](img/plot.png)

You may save this plot to a PDF of PNG file.

```R
p + ggsave("plot.pdf", width=9,height=9)
p + ggsave("plot.png", width=9,height=9)
```
## Analyzing data

Besides plotting charts you may analyze the data using functions provided by this library. Below there are some examples of such usage.

Get top 10 countries by query number. The result shows total query number (`cc_total_cnt`) from a country and its share in total number of queries (`cc_total_perc`).

```R
get_top_cnt(df_traffic, cc, 10, FALSE)
# A tibble: 10 x 3
   cc    cc_total_cnt cc_total_perc
   <chr>        <dbl>         <dbl>
 1 CZ      4610188606        0.294 
 2 US      3807472763        0.243 
 3 NL       815444806        0.0521
 4 DE       636126409        0.0406
 5 RU       493394969        0.0315
 6 IE       444292037        0.0284
 7 SG       392071823        0.0250
 8 BE       379326849        0.0242
 9 CN       372472040        0.0238
10 SK       323311308        0.0206
```

Get top continents by query number.

```R
get_top_cnt(df_traffic, continent, Inf, FALSE)
# A tibble: 5 x 3
  continent continent_total_cnt continent_total_perc
  <chr>                   <dbl>                <dbl>
1 Europe             9367938129              0.598  
2 Americas           4362960402              0.279  
3 Asia               1709276824              0.109  
4 Oceania              98610911              0.00630
5 Africa               68067637              0.00435
```

Get top 50 regions by query number.

```R
get_top_cnt(df_traffic, region, 50, FALSE)
# A tibble: 22 x 3
   region             region_total_cnt region_total_perc
   <chr>                         <dbl>             <dbl>
 1 Eastern Europe           5707631672           0.364  
 2 Northern America         3957261938           0.253  
 3 Western Europe           2282760564           0.146  
 4 Northern Europe          1145238027           0.0731 
 5 Eastern Asia              899224585           0.0574 
 6 South-Eastern Asia        567955853           0.0363 
 7 South America             365281294           0.0233 
 8 Southern Europe           232307866           0.0148 
 9 Western Asia              114283049           0.00730
10 Southern Asia             112986698           0.00722
# … with 12 more rows
```

Get top 10 regions by query number along with evaluated RTT for each region.

```R
get_wm_rtt(df_traffic, df_rtt, "region", 10)
Joining, by = c("server", "ipv", "region")
# A tibble: 10 x 4
   region             rtt_median_wm_region region_total_cnt region_total_perc
   <chr>                             <dbl>            <dbl>             <dbl>
 1 Eastern Europe                     31.9       5707631672           0.364  
 2 Northern America                   99.0       3957261938           0.253  
 3 Western Europe                     18.6       2282760564           0.146  
 4 Northern Europe                    38.0       1145238027           0.0731 
 5 Eastern Asia                      255.         899224585           0.0574 
 6 South-Eastern Asia                220.         567955853           0.0363 
 7 South America                     122.         365281294           0.0233 
 8 Southern Europe                    47.1        232307866           0.0148 
 9 Western Asia                       72.9        114283049           0.00730
10 Southern Asia                     161.         112986698           0.00722
```

Get top countries with best evaluated RTT.

```R
get_wm_rtt(df_traffic, df_rtt, "cc", Inf) %>% arrange(rtt_median_wm_cc)
Joining, by = c("server", "cc", "ipv")
# A tibble: 239 x 4
   cc    rtt_median_wm_cc cc_total_cnt cc_total_perc
   <chr>            <dbl>        <dbl>         <dbl>
 1 CZ                8.77   4610188606      0.294   
 2 AT               11.1      89448591      0.00571 
 3 SK               11.4     323311308      0.0206  
 4 BE               12.4     379326849      0.0242  
 5 NL               13.9     815444806      0.0521  
 6 DE               15.6     636126409      0.0406  
 7 SE               16.8      83310521      0.00532 
 8 LU               16.9       8348772      0.000533
 9 HR               16.9      13171833      0.000841
10 SI               17.3       8951270      0.000572
# … with 229 more rows
```