#!/usr/bin/env python

import geoip2.database
import sys
import csv

#sys.setdefaultencoding("utf-8")

fin = sys.argv[1]
fout = sys.argv[2]
server = sys.argv[3]
ipv = sys.argv[4]

## MaxMind GeoIP
#
# in case of using free version of MaxMind DB (https://dev.maxmind.com/geoip/geoip2/geolite2/):
# geoip_db_cc = "/var/lib/GeoIP/GeoLite2-Country.mmdb"
# geoip_db_asn = "/var/lib/GeoIP/GeoLite2-ASN.mmdb"
#
geoip_db_cc = "/var/lib/GeoIP/GeoIP2-Country.mmdb"
geoip_db_asn = "/var/lib/GeoIP/GeoIP2-ISP.mmdb"
#

with geoip2.database.Reader(geoip_db_asn) as reader_asn:
	with geoip2.database.Reader(geoip_db_cc) as reader_cc:
		with open(fin, 'rb') as csvin:
			csvr = csv.DictReader(csvin, delimiter=';', fieldnames=['time','src','rtt'])
			with open(fout, 'wb') as csvout:
				fn = csvr.fieldnames
				fn.append('cc')
				fn.append('asn')
				fn.append('server')
				fn.append('ipv')
				csvw = csv.DictWriter(csvout, delimiter=';', fieldnames=fn)
				for row in csvr:
					ip=row['src']
					try:
						res_asn = reader_asn.isp(ip)
						asn = res_asn.autonomous_system_number
						asn_descr = res_asn.autonomous_system_organization.rstrip('\n')
					except:
						asn = ""
						asn_descr = ""
					try:
						res_cc = reader_cc.country(ip)
						cc = res_cc.country.iso_code
					except:
						cc = ""
					row['cc']=cc
					row['asn']=asn
					row['server']=server
					row['ipv']=ipv
					csvw.writerow(row)
